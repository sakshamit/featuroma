

#Create db
CREATE DATABASE featuroma

#Badges
CREATE TABLE Badges (
	 id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	 name varchar(255) NOT NULL UNIQUE
);

#Tags
CREATE TABLE Tags (
 id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
 name varchar(255) NOT NULL UNIQUE
);

#Categories
CREATE TABLE Categories (
 id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
 name varchar(255) NOT NULL UNIQUE
);

#Languages
CREATE TABLE Languages (
 id INTEGER PRIMARY KEY AUTO_INCREMENT,
 name varchar(255) UNIQUE
);

#PriceTypes
CREATE TABLE PriceTypes (
 id INTEGER PRIMARY KEY AUTO_INCREMENT,
 name varchar(255) UNIQUE
);
#DocumentTypes
CREATE TABLE DocumentTypes (
 id INTEGER PRIMARY KEY AUTO_INCREMENT,
 name varchar(255) UNIQUE
);

#Users
CREATE TABLE Users (
 id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
 email_id varchar(255) NOT NULL UNIQUE,
 referred_by_user_id INTEGER,
 name varchar(255) NOT NULL,
 rating INTEGER,
 status INTEGER,
 date_created DATETIME DEFAULT   CURRENT_TIMESTAMP,
 date_modified DATETIME ON UPDATE CURRENT_TIMESTAMP,
 badge_id INTEGER,
 FOREIGN KEY(referred_by_user_id) REFERENCES Users(id),
 FOREIGN KEY(badge_id) REFERENCES Badges(id)
);

#ProjectsHosted
CREATE TABLE ProjectsHosted (
 id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
 title varchar(255) NOT NULL,
 description TEXT,
 date_created DATETIME DEFAULT   CURRENT_TIMESTAMP,
 date_modified DATETIME ON UPDATE CURRENT_TIMESTAMP,
 project_hosted_by_uid INTEGER,
 status INTEGER,
 FOREIGN KEY(project_hosted_by_uid) REFERENCES Users(id)
);

#Videos
CREATE TABLE Videos (
 id INTEGER PRIMARY KEY AUTO_INCREMENT,
 title varchar(255) NOT NULL,
 description TEXT, 
 date_created DATETIME DEFAULT   CURRENT_TIMESTAMP,
 event_date DATETIME,
 event_location varchar(255),
 video_url varchar(255),
 perma_url varchar(255),
 length Long, 
 language_id INTEGER,
 price INTEGER,
 price_type INTEGER,
 hosted_for_project_id INTEGER,
 submitted_by_user_id  INTEGER,
 category_id  INTEGER,
 thumbnail_url varchar(255),
 status  INTEGER,
 verified_by_uid INTEGER,
 date_modified DATETIME ON UPDATE CURRENT_TIMESTAMP,
 date_verified DATETIME,
 FOREIGN KEY(language_id) REFERENCES Languages(id),
 FOREIGN KEY(price_type) REFERENCES PriceTypes(id),
 FOREIGN KEY(hosted_for_project_id) REFERENCES ProjectsHosted(id),
 FOREIGN KEY(submitted_by_user_id) REFERENCES Users(id),
 FOREIGN KEY(category_id) REFERENCES Categories(id),
 FOREIGN KEY(verified_by_uid) REFERENCES Users(id)
);

#Documents
CREATE TABLE Documents (
 id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
 name varchar(255) NOT NULL,
 description TEXT,
 date_created DATETIME DEFAULT   CURRENT_TIMESTAMP,
 date_modified DATETIME ON UPDATE CURRENT_TIMESTAMP,
 video_id INTEGER,
 type_id INTEGER,
 path_url varchar(255),
 FOREIGN KEY(video_id) REFERENCES Videos(id),
 FOREIGN KEY(type_id) REFERENCES DocumentTypes(id)
);

#VideoLicensed
CREATE TABLE VideoLicensed (
 id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
 video_id INTEGER NOT NULL,
 licensed_to_uid INTEGER NOT NULL,
 comment TEXT,
 date_created DATETIME DEFAULT   CURRENT_TIMESTAMP,
 date_modified DATETIME ON UPDATE CURRENT_TIMESTAMP,
 FOREIGN KEY(video_id) REFERENCES Videos(id),
 FOREIGN KEY(licensed_to_uid) REFERENCES Users(id)
);

#TagVideos
CREATE TABLE TagVideos (
 id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
 tag_id INTEGER NOT NULL,
 video_id INTEGER NOT NULL,
 date_created DATETIME DEFAULT   CURRENT_TIMESTAMP,
 date_modified DATETIME ON UPDATE CURRENT_TIMESTAMP,
 FOREIGN KEY(tag_id) REFERENCES Tags(id),
 FOREIGN KEY(video_id) REFERENCES Videos(id)
);