<?php


class DataBase {
	private $dbHost     = "localhost";
	private $dbUsername = "root";
	private $dbPassword = "root";
	private $dbName     = "featuroma";
	private $result = array();
	
	function __construct(){
		//echo print_r('under const of db, next is assigning');
		if(!isset($this->db)){
			//echo 'db is not set,assinging now';
			$conn = new mysqli($this->dbHost, $this->dbUsername, $this->dbPassword, $this->dbName);
			if(isset($conn)){
				//echo 'conn is set';
			}else{
				//echo 'conn is not set';
			}
			if($conn->connect_error){
				echo print_r('connection error');
				die("Failed to connect with MySQL: " . $conn->connect_error);
			}else{
				//echo print_r('succ!');
				$this->db = $conn;
			}
		}else{
			//echo print_r('db is set');
		}
	}

	private function getReturnValue($isSuc){
		if($isSuc==TRUE){
			$this->result['status']="success";
		}else{
			$this->result['status']="failure";
		}
		return json_encode($this->result);
	}
	public function submitVideoForSale($param){
		try{
			//check for email-id
			//echo print_r($param);
			$emailId = $param['emailId'];
			$userJson = $this->getUserDetails($emailId);
			if($userJson==null || $userJson['id']==null || $userJson['id']==""){
				//sign up.
				$userSignUpResp = $this->signup($param);
				if($userSignUpResp['status']=="failure"){
					return  $this->getReturnValue(false);
				}
				$userJson=$userSignUpResp->details;
			}
			
			if($userJson==null || $userJson['id']==null || $userJson['id']==""){
				return  $this->getReturnValue(false);
			}
			$ownerId = $userJson['id'];
			$embedUrl =str_replace("watch?v=", "embed/", $param['video_url']);

			$origUrl =  $param['video_url'];
			$pos = strpos($origUrl, "watch?v=") + 8;
		    $uVideoid= substr($origUrl,$pos);
			//echo $id."\n";
 			$thumbnail=str_replace("xRJozTP8xYQ", $uVideoid,"http://i3.ytimg.com/vi/xRJozTP8xYQ/hqdefault.jpg"); 
			//echo "thumbnail_url=".$thumbnail;
			$query = "INSERT INTO Videos (title,description,video_url,thumbnail_url, submitted_by_user_id, status) 
				values('{$param['name']}', '{$param['about_video']}', '{$embedUrl}', '{$thumbnail}', '{$ownerId}', 2)";
			$this->log4Php('submitVideoForSale>insert>',$query);
			$insert = $this->db->query($query);
			if($insert===TRUE){
				return $this->getReturnValue(true);
			}else{
				return $this->getReturnValue(false);
			}
		} 
		catch (Exception $e) {
				$this->log4Php('createPlaylist>error>',$e->getMessage());
				echo $e->getMessage() ;
				exit;
		}	
	}

	private function signup($param){
		$this->result['status']="success";
		if($param['emailId']==null || $param['emailId']==""){
			$this->result['status']="failure";
			$this->result['reason']="Email id can not be empty.";
		}
		if($param['name']==null || $param['name']==""){
			$this->result['status']="failure";
			$this->result['reason']=$result->$this->result['reason'] . " Email id can not be empty.";
		}
		if($this->result['status']=="failure"){
			return json_encode($result);
		}
		$query = "INSERT INTO Users(email, name) values('{$param['emailId']}', '{$param['name']}'')";
		try{
			$insert = $this->db->query($query);
			if($insert===TRUE){
				$result->details=getUserDetails($param['emailId']);
				return json_encode($result);
			}else{
				$this->result['status']="failure";
				return $this->result ;
			}
		}catch(Exception $e){
			$this->log4Php('createPlaylist>error>',$e->getMessage());
			//echo $e->getMessage() ;
			$this->result['status']="failure";
			return  json_encode($result);
		}

	}

	public function getUserDetails($emailId){
		$TAG = 'getUserDetails::' ;
		$this->log4Php($TAG.'>Method>','',1);
		try{
			//$query = "select * from Users where email_id=".$emailId.";";
			$query = "select * from Users where email_id='{$emailId}';";
			$this->log4Php($TAG.'>query>',$query);
			$queryresult = $this->db->query($query);
			
			if($queryresult)
				return $queryresult->fetch_assoc();
			else
				return ;

		} catch (Exception $e) {
			$this->log4Php($TAG.'>error>',$e->getMessage());
			echo $e->getMessage() ;
			return null;
		}
	}
 
	public function licenseVideo($param){
		try{
			try{
			//check for email-id
			$emailId = $param['emailId'];
			$userJson = $this->getUserDetails($emailId);
			/*echo "\n\r"."id of the user:";
			echo print_r($userJson);
 			echo "\n\r";*/
			if($userJson==null || $userJson['id']==null || $userJson['id']==""){
				//sign up.
				$userSignUpResp = $this->signup($param);
				if($userSignUpResp['status']=="failure"){
					return  $this->getReturnValue(false);
				}
				$userJson=$userSignUpResp->details;
			}
			
			if($userJson==null || $userJson['id']==null || $userJson['id']==""){
				return  $this->getReturnValue(false);
			}
			$buyerId = $userJson['id'];
		
			
 			$query = "INSERT INTO VideoLicensed (video_id,licensed_to_uid,comment) 
				values('{$param['video_id']}', '{$buyerId}', '{$param['message']}');";
			$this->log4Php('licenseVideo>insert>',$query);
			$insert = $this->db->query($query);
			if($insert===TRUE){
				return  $this->getReturnValue(true);
			}else{
				return  $this->getReturnValue(false);
			}
		} 
		catch (Exception $e) {
				$this->log4Php('createPlaylist>error>',$e->getMessage());
				echo $e->getMessage() ;
				exit;
		}	
		}
		catch (Exception $e) {
			$this->log('createPlaylist>error>',$e->getMessage());
			echo $e->getMessage() ;
			exit;
		}		
	}

	private function login($param){
	
	}
	function isUserRegistred($user){
	
	}
	
	private function getRows($query){
		$TAG = 'getRows::' ;
		$this->log4Php($TAG.'query>', $query, 1) ;
			
		$this->log($TAG." qyery ",$query);
	
		$result2 = $this->db->query($query);
		$rows = array();
		if(!$result2)
			return $maxPosition;
				
			while (($row = $result2->fetch_assoc()) !== null){
				$rows[] = $row;
			}
			$this->log4Php($TAG.'rows>', $rows, 1) ;
			return $rows ;
	}
	function getCount($query){
		$TAG = 'getCount::' ;
			
		$this->log4Php($TAG.'>query>',$query,1);
		
		$result = $this->db->query($query);
		$this->log($TAG,$result);
		$rowCount = 0;
		
		while (($row = $result->fetch_assoc()) !== null){
			$rowCount =  $row['count'] ;
		}
		
		return $rowCount ;
	}
	
	
	
	function addTagForVideo($videoId,$tags){
	
	}
	
	function getLanguage(){
		$TAG = 'getLanguage::' ;
		$this->log4Php($TAG.'>Method>','',1);
		try{
			$query = "select name from language order by name";
			$this->log($TAG.'>query>',$query);
			$queryresult = $this->db->query($query);
			$index = 0 ;
			$result = array() ;
			//while ($row = $queryresult->fetch_assoc()){
			while (($row = $queryresult->fetch_assoc()) !== null){
				$result[]  = $row["name"] ;
			}
			return $result ;
		} catch (Exception $e) {
			$this->log($TAG.'>error>',$e->getMessage());
			echo $e->getMessage() ;
			exit;
		}
	}
	function getTags(){
		$TAG = 'getTags::' ;
		$this->log4Php($TAG.'>Method>','',1);
		try{
			$query = "select name from tags order by name";
			$this->log($TAG.'>query>',$query);
			$queryresult = $this->db->query($query);
			$index = 0 ;
			$result = array() ;
			//while ($row = $queryresult->fetch_assoc()){
			while (($row = $queryresult->fetch_assoc()) !== null){
				$result[]  = $row["name"] ;
			}
			return $result ;
		} catch (Exception $e) {
			$this->log($TAG.'>error>',$e->getMessage());
			echo $e->getMessage() ;
			exit;
		}
	}
	function getAllCategories(){
		$TAG = 'getCategories::' ;
		$this->log4Php($TAG.'>Method>','',1);
		try{
			$query = "select * from categories order by name";
			//$this->log($TAG.'>query>',$query);
			$queryresult = $this->db->query($query);
			$resultArray = array() ;
			//while ($row = $queryresult->fetch_assoc()){
			while (($row = $queryresult->fetch_assoc()) !== null){
				$resultArray[]  = $row ;
			}
			$result['categories']=$resultArray;
			return $result ;
		} catch (Exception $e) {
			$this->log($TAG.'>error>',$e->getMessage());
			echo $e->getMessage() ;
			exit;
		}
	}
	function search($param){
		$TAG = 'search::' ;
		$this->log4Php($TAG.'>Method>','',1);
		try{

			$category_id = $param['category_id'];
			$categoryStatement = "";
			if($category_id!=null && $category_id!=""){
				$categoryStatement = " and category_id = ".$category_id;
			}

			$textQuery = $param['query'];
			$likeStatement="";
			//todo : saksham change this logic to consider all space separated words. Currently it will fail.
			if($textQuery!=""){
				$likeStatement = " and title like '%".$textQuery."%' "; 
			}

			//echo 'under search';
			$query = "select id, title, thumbnail_url 
					from Videos 
					where status=2".$likeStatement.$categoryStatement." 
					order by date_created desc;";
			//echo $query;
			$this->log4Php($TAG.'>query>',$query);
			$queryresult = $this->db->query($query);
			$index = 0 ;
			$result = array() ;
			//while ($row = $queryresult->fetch_assoc()){
			while (($row = $queryresult->fetch_assoc()) !== null){
				$result[]  = $row ;
			}
			return $result ;
		} catch (Exception $e) {
			$this->log($TAG.'>error>',$e->getMessage());
			echo $e->getMessage() ;
			exit;
		}
	}


	function getVideoDetails($param){
		$TAG = 'getVideoDetails::' ;
		$this->log4Php($TAG.'>Method>','',1);
		try{
			$videoId = $param['id'];
			$query = "select Videos.id as video_id, Videos.event_location, Videos.title as video_title, Videos.description as video_desc,Videos.video_url, 
			category_id, Categories.name  as category_name, thumbnail_url
			 language_id,Languages.name as language, hosted_for_project_id, ProjectsHosted.title as project_title 
				 from
				 Videos join Categories on Videos.category_id=Categories.id join Languages on Languages.id=language_id left join ProjectsHosted on hosted_for_project_id=ProjectsHosted.id
				 where Videos.id=".$videoId.";";
			$this->log4Php($TAG.'>query>',$query);
			$queryresult = $this->db->query($query);
			$index = 0 ;
			$result = array() ;
			//while ($row = $queryresult->fetch_assoc()){
			while (($row = $queryresult->fetch_assoc()) !== null){
				$result[]  = $row ;
			}
			//$result[0]['hosted_for_project_id'] = 2;
			//$result[0]['project_title'] = "Project_2";
			return $result ;
		} catch (Exception $e) {
			$this->log($TAG.'>error>',$e->getMessage());
			echo $e->getMessage() ;
			exit;
		}
	}
	
	private function log4Php($tag,$log){
		if(true) return;
		echo $tag.'\n';
		echo print_r($log);
		
	}
	private function validateUser($userId){
		$TAG = 'validateUser::' ;
	
		$sql = "select id from pm_users where id =". $userId;
			
		$this->log4Php($TAG.'>query>',$sql,1);
	
		if(empty($userId))
		{
			$this->log($TAG.' $sql','  INVAIND USER');
			return  false ;
		}
		$TAG = 'validateUser' ;
	
		$result = $this->db->query($sql);
		$this->log($TAG,$result);
		$rowCount = 0;
	
		while (($row = $result->fetch_assoc()) !== null){
			$rowCount++;
		}
	
		if($rowCount>0){
			$this->log($TAG.' $sql','  VAIND USER');
			return true ;
		}
		else{
			$this->log($TAG.' $sql','  INVAIND USER');
			return false ;
		}
	
	}		
}
?>
