var span = document.getElementsByClassName("close")[0];
var modal = document.getElementById('myModal');

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

function alert(message){
	$('.alert-message').html(message);
	modal.style.display = "block";
}