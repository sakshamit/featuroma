<div class="navbar navbar-default main-top-nav" role="banner" >
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle" data-target=".bs-navbar-collapse"
				data-toggle="collapse" type="button">
				<span class="sr-only"> Toggle navigation </span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<div class="header section" id="blog-title">
				<div class="widget Header" data-version="1" id="Header1">
					<div id="header-inner">
						<a href="index.php" style="display: block;margin-left:15px;"> <img alt="Featuroma"
							height="52px; " id="Header1_headerimg"
							src="../img/logo_white.png" style="display: block"
							width="200px; ">
						</a>
					</div>
				</div>
			</div>
		</div>
		<nav class="collapse navbar-collapse bs-navbar-collapse navbar-right"
			role="navigation">
			<ul class="nav navbar-nav">
				<li><a data-toggle="collapse" data-target=".in" href="how_it_works.php"> How it works? </a></li>
				<li><a data-toggle="collapse" data-target=".in" href="about.php"> About Us</a></li>
				<li><a data-toggle="collapse" data-target=".in" href="mailto:featuroma17@gmail.com"> Contact Us </a></li>
				<!-- <li><a href="#" class = 'cart' > Cart(0) </a></li> -->
				
			</ul>
		</nav>
	</div>
</div>
   
<!-- Modal content -->
<div id="myModal" class="overlay" style="display: none;">
<div class="modal-content" >
  <div class="modal-header">
    <span class="close">&times;</span>
    <h2>Message</h2>
  </div>
  <div class="modal-body">
    <p class="alert-message"></p>
   
  </div>
 
</div>
</div>

