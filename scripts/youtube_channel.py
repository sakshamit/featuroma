import urllib2
import json
import xlwt
import csv
#content = urllib2.urlopen("https://www.googleapis.com/youtube/v3/search?key=AIzaSyAFvZb03zjoUo5hwcNBf4CoBs05uKwvY2o&channelId=UCZvRm2DBvucMek6a3RBRulA&part=snippet,id&order=date&maxResults=20").read()
#print content

APPLICATION_KEY='AIzaSyAFvZb03zjoUo5hwcNBf4CoBs05uKwvY2o';
DATA_CSV_FILE='/Library/WebServer/Documents/featuroma/featuroma/scripts/data.csv';

#channelIds = ['UC4j7lvITyz4ZbyRwdUfBKV','UCZvRm2DBvucMek6a3RBRulA']
channelIds = ['UCzVDfh5rorb4zqkezA8qH0g', 'UCLz-5KdjrNZGdenYfIvt-dw', 'UCZvRm2DBvucMek6a3RBRulA', 'UCwDhnERqftGr2agaAeN8zRg','UC4GOw4F29NgZKq_0hnMudxw','UCnXlk4_wipvTNvDoMGD1ORQ']

for channelId in channelIds:
	#print 'looking for channel id : '+channelId
	videoIdsFromChannelUrl="https://www.googleapis.com/youtube/v3/search?key="+APPLICATION_KEY+"&channelId="+channelId+"&part=snippet,id&order=date&maxResults=50&fields=items(id(videoId))"
	#print videoIdsFromChannelUrl
	response = urllib2.urlopen(videoIdsFromChannelUrl).read()
	#print response
	videoIds=""
	videoIdListJson=json.loads(response)
	for item in videoIdListJson['items']:
	 	videoId=item['id']['videoId']
		#print "Getting details for : "+videoId
		videoIds=videoIds+videoId+","		
	#print videoIds
	videoDetailUrl="https://www.googleapis.com/youtube/v3/videos?id="+videoIds+"&key="+APPLICATION_KEY+"&part=snippet,statistics&fields=items(snippet,id)"
	print videoDetailUrl
	response = urllib2.urlopen(videoDetailUrl).read()
	# print response
	videoDetailJson=json.loads(response)
	channelTitleWritten=False
	for item in videoDetailJson['items']:
		detailJson=item['snippet']
		title=detailJson['title']
		desc=detailJson['description']
		videoUrl="https://www.youtube.com/watch?v="+item['id']
		channelTitle=detailJson['channelTitle']
		#print "Channel title"+channelTitle
		w = csv.writer(open(DATA_CSV_FILE,"a"))
		if(channelTitleWritten==False):
			w.writerow([channelTitle])
			channelTitleWritten=True
		w.writerow([title.encode('utf-8').strip(), desc.encode('utf-8'), videoUrl])
		#print title+",'"+desc+"',"+videoUrl