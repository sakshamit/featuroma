INSERT INTO Categories (name)
VALUES ('War and Peace');
#

INSERT INTO Languages (name)
VALUES ('English');
#
INSERT INTO PriceTypes (name)
VALUES ('Dollar USA');

#
INSERT INTO ProjectsHosted (title, description,project_hosted_by_uid, status)
 VALUES ('UP Election Coverage from Kanpur', 'Need 24x7 election coverage from kanpur on election date', 1, 1);

INSERT INTO ProjectsHosted (title, description,project_hosted_by_uid, status)
 VALUES ('Sasikala and Panneerselvam fight', 'It should be unbiased.', 1, 0);
#

##videos
INSERT INTO Videos (title, description,video_url, language_id, submitted_by_user_id, category_id, 
	thumbnail_url, status)
 VALUES ('Surviving Boko Haram From Nigeria To Cameroon',
  'A massive humanitarian crisis is boiling in Africa\'s Lake Chad basin region, stemming from violence waged by Boko Haram. More than 2.6 million people are displaced across the region.', 
  'https://www.youtube.com/watch?v=xRJozTP8xYQ', 1, 1, 2, 'http://i3.ytimg.com/vi/xRJozTP8xYQ/hqdefault.jpg', 2);

  ##

  INSERT INTO Videos (title, description,video_url, language_id, submitted_by_user_id, category_id, 
	thumbnail_url, status)
 VALUES ('Challenges Of Education In Rural India',
  'VV-PACS Community Correspondent Mister Alam from Nalanda district of Bihar reports on the lack of school teachers in Chakjohara Primary school. There is only 1 teacher since 2007 who was also put on BLO duty during the Lok Sabha elections due to which the school was shut for 3 days. ', 
  'https://www.youtube.com/watch?v=NOTue7W78N8', 1, 1, 1, 'http://i3.ytimg.com/vi/NOTue7W78N8/hqdefault.jpg', 2);

UPDATE Videos SET video_url='https://www.youtube.com/embed/xRJozTP8xYQ' where id=1