

<!DOCTYPE html>
<!-- saved from url=(0032)http://vubetube-pro.blogspot.in/ -->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:b="http://www.google.com/2005/gml/b"
	xmlns:data="http://www.google.com/2005/gml/data"
	xmlns:expr="http://www.google.com/2005/gml/expr">

<script>
  function resizeIframe(obj) {
  		$('iframe').css('width',(window.innerWidth/2)+100);
   		$('iframe').css('height',(window.innerWidth/4)+100);
  }
</script>

<?php include_once 'header_tag.php';?>
<body id="body-templates">

	<?php include_once 'header.php'; ?>
	<!--end top-page-menu-->
	<div class="container" id="container">
		<div class="row-content">

			<div class="row">
				<div class="col-md-12">
					<div id="video_wrapper"></div>
					<div class="magazine-main section" id="magazine-slide-wrapper">
						<div class="widget HTML" data-version="1" id="HTML6">
							<div class="widget-content">
								<div class="magazine_slider_flex"></div>
							</div>

							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-recent">
				<div class="main-post-div" style="width: 100%;min-height: 500px">
					<div class="main section" id="main">
						<div class="widget Blog" data-version="1" id="Blog1">

							<div class="date-outer">

								<div class="date-posts">

									<div class="post-outer-templet" style="display: none"
										align="center">
										<div>
											<div>
												<h2 class='video-title'></h2>
											</div>
											<div class="post hentry thumbnail">

												<div class="play-link" id='play-link'>													
													<iframe class="play-link-video"
														frameborder="0" allowfullscreen  onload="resizeIframe(this)"> </iframe>
												</div>

												<div id="video-details">
													<div class='descreption' align="justify">descreption</div>
													<div class='event_location' align="left">Event location</div>
													<div class='category' align="left">Category</div>
													<div class='tags' align="left">Tags</div>
													<div id='language' align="left">Language</div>
													<div id='project' align="left">Project</div>
												</div>
												<div style="clear: both;"></div>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
					<!-- END Loop -->
				</div>
				<!-- END Content -->
				<div class="col-md-12">
					<div class="sidebox">
						<div class="sidebar section" id="sidebar">
							<div class="widget HTML" data-version="1" id="HTML1">
								<h2 class="title">License this video</h2>
								<div class="widget-content" align="center" style="width: 300px">
									<!-- <form class="email-well"  method="post"> -->
									<fieldset>
										<div class="form-group">
											<input id="email" name="email" placeholder="Email address"
												type="text" class="form-control">
											<input id="name" name="name" placeholder="Name"
												type="text" class="form-control">
											
											<textarea rows="4" cols="50" class="form-control"
												id="message" placeholder="Message"></textarea>
										</div>
										<div class="form-group">
											<input id="license_this_video" type="submit" value="Send"
												class="btn featuroma-button btn-block">
										</div>
									</fieldset>
									<!-- </form> -->
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Container -->
	<?php include_once 'footer.php'; ?>


</body>


<script type="text/javascript">

var video_id = <?php echo "'".$_GET['id']."'";?> ;
var video_url = <?php echo "'".$_GET['video_url']."'";?> ;
$(document).ready(function() {
	loadFile();

	});



var json ;
function loadFile() {
	  $.ajax({
	       url: "api.php?_api_call=get_video_details&id="+video_id,
	           //force to handle it as text
	       dataType: "json",
	            success: function (json) {

	                  {
	                      
	                      var card = $('.post-outer-templet');
	                      card.show();
	                      	 card.find('.video-title').html(json.video_title);
	                      	 card.find('.play-link-video').attr('src',json.video_url+"?autoplay=0&cc_load_policy=1&showinfo=0");
	                      	 card.find('.video-thumb').attr('src','http://i3.ytimg.com/vi/fx7giKNnSuw/hqdefault.jpg');
	                      	 card.find('.add-to-cart').attr('data',JSON.stringify(json));
	                      	 card.find('.descreption').html("<strong>Description:</strong>&nbsp;"+json.video_desc);
	                      	 
	                      	 if(json.event_location==undefined || json.event_location=="null" || json.event_location==""){
	                      	 	card.find('.event_location').html("");
	                      	 }else{
	                      	 	card.find('.event_location').html("<strong>Location:</strong>&nbsp;"+json.event_location);	                      
	                      	 }
	  
	                      	//card.find('.perma_url').html(""+json.perma_url);
	                      	if(json.category_name==undefined || json.category_name=="null" || json.category_name==""){
	                      		card.find('.category').html("");
	                      	}else{
	                      		card.find('.category').html(
	                      			"<strong>Category:</strong>&nbsp;"
	                      			+"<a href=\"searchresult.php?category_id=" + json.category_id +"&category_name="+json.category_name+"\">"
	                      			+json.category_name
	                      			+"</a>");
	                      	}
	           				/*if(json.tags==undefined || json.tags=="null" || json.tags==""){
	                      		card.find('.category').html("");
	                      	}else{
	                      		var res = json.tags.split(",");
		                   		card.find('.tags').html("<strong>Tags:</strong>");
		                      	for(var i =1 ;i<res.length;i++){
		                      		card.find('.tags').html(card.find('.tags').html()+"&nbsp;<span class='label label-danger'>"+res[i]+"</span>");
		                      	}
	                      	}*/

	                      	//Language
	                      	if(json.language==undefined || json.language=="null" || json.language==""){
	                      		card.find('#language').html("");
	                      	}else{
	                      		card.find('#language').html(
	                      			"<strong>Language:</strong>&nbsp;"
	                      			+json.language);
	                      	}

	                      	if(json.project_title==undefined || json.project_title=="null" || json.project_title==""){
	                      		card.find('#project').html("");
	                      	}else{
	                      		card.find('#project').html(
	                      			"<strong>Project:</strong>&nbsp;"
	                      			+"<a href=\"searchresult.php?project_id=" + json.hosted_for_project_id +"\">"
	                      			+json.project_title
	                      			+"</a>");
	                      	}
	                      		                      		                 
	                      	
	                      }	                  
	             }
	  });
	
}

$( ".home" ).click(function() {
	  location.href = "index.php";
	});

function ValidateEmail(mail)   
{  
	 var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
  
} 
$( "#license_this_video" ).click(function() {

	if ($('#email').val().length == 0
			|| $('#message').val().length == 0
			
			) {
	     alert("Please fill all information!");
	     return;
	}

// 	if(!ValidateEmail($('#email'))){
// 		 alert("Invalid email address!");
// 		 return;
// 	}
	responseObject = {
            "email"      :$('#email').val(),
            "video_id"      :video_id,
            "message"      :$('#message').val(),
            "name"      :$('#name').val()
                         
		}


	 var url="api.php?_api_call=license.video"
	      $.ajax({

	          url:url,
	          type: "POST",
	          dataType: "json",
	          data:responseObject,
	           success: function (msg) {  

				var jsonObject= $.parseJSON(msg);

	           	if(jsonObject.status=='success'){
	              		alert("Sucessfully submited!");
	              		 $('#email').val('');
	              		 $('#message').val('');
		           }
		           else  
		        	   //alert(jsonObject.reason);		           	             	             
		        		alert("Something went wrong!!");		           	             	             
	          },
	          error: function (jqXHR, exception) {
	        	     var msg = '';
	        	     if (jqXHR.status == 302) {
	        	        
	        	      }
	        	     if (jqXHR.status === 0) {
	        	         msg = 'Not connect.\n Verify Network.';
	        	     } else if (jqXHR.status == 404) {
	        	         msg = 'Requested page not found. [404]';
	        	     } else if (jqXHR.status == 500) {
	        	         msg = 'Internal Server Error [500].';
	        	     } else if (exception === 'parsererror') {
	        	         msg = 'Requested JSON parse failed.';
	        	     } else if (exception === 'timeout') {
	        	         msg = 'Time out error.';
	        	     } else if (exception === 'abort') {
	        	         msg = 'Ajax request aborted.';
	        	     } else {
	        	         msg = 'Uncaught Error.\n' + jqXHR.responseText;
	        	     }
	        	    console.log("error"+msg);
	        	   },

	      });
    
	});  
</script>


</html>
